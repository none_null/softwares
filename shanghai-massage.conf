# 监听80端口，把所有的http请求通过返回301值重定向到https上
server {
    listen 80;              # IPv4
    listen [::]:80;         # IPv6
    server_name shanghai-massage.club;             # 服务器配置名
    return 301 https://$host$request_uri;  # http请求全都通过301重定向到同名的https URL上
}
# 监听443端口，提供https服务
server {
    listen 443 ssl;                        # 监听443端口，且开启ssl
    listen [::]:443 ssl;                   # v6

    # 使用的SSL 证书，这里用之前letsEncrypt签发证书存放的位置
    ssl_certificate /etc/letsencrypt/live/shanghai-massage.club/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/shanghai-massage.club/privkey.pem;

    # 复用SSL连接
    ssl_session_cache          shared:SSL:1m;
    ssl_session_timeout        5m;

    # SSL加密方式这里用的是nginx的默认设置
    ssl_ciphers                HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers  on;

    ssl_protocols TLSv1.2;                 # 只支持TLS v1.2协议

    # 访问日志
    access_log  /var/log/nginx/host.access.log;
    error_log   /var/log/nginx/host.error.log;

    # 开启 HSTS 支持
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;

    server_name shanghai-massage.club;
    client_max_body_size 100M;
    location = /favicon.ico {
        access_log off;
        log_not_found off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    root /var/www/html/wordpress;

    index  index.php index.html index.htm;

    location / {        
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
         include snippets/fastcgi-php.conf;
        # With php-fpm (or other unix sockets):
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        include fastcgi_params;
    }
}

